<?php
/**
 * Created by PhpStorm.
 * remove expired keys from memcache.
 * User: fushanlang
 * Date: 13-10-11
 * Time: 下午4:21
 */

class mem_dtor extends Memcache
{
    private $server_id;
    public function __construct($host,$port)
    {
        $this->server_id = "$host:$port";
        $this->connect($host,$port);
    }


    public function gc()
    {
        $t = time();
        $_this = $this;
        $func = function($key,$info) use ($t,$_this)
        {
            if($info[1] - $t < -30)
            {
                $_this->delete($key);
            }
        };
        $this->lists($func);
    }


    public function info()
    {
        $t = time();
        $func = function($key,$info) use ($t)
        {
            echo $key,' => Exp:',$info[1] - $t,"\n";
        };
        $this->lists($func);
    }

    private function lists($func)
    {
        $sid = $this->server_id;
        $items                        = $this->getExtendedStats('items');
        foreach($items[$sid]['items'] as $slab_id => $slab)
        {
            $item        = $this->getExtendedStats('cachedump',$slab_id,0);
            foreach($item[$sid] as $key => $info)
            {
                $func($key,$info);
            }
        }
    }
}

$mem = new mem_dtor('127.0.0.1',11211);
$mem->info();
$mem->gc();